«/* 
	---------------------------------------------------------- 
	--- CONFIG SECTION --------------------------------------- 
	---------------------------------------------------------- 
	Populate the lookup map in the envelope with project
	wide settings, which can be overridden in the individual
	definition files										
	Also: define the default import list entries  
*/»

«/*prefix
lookup generateGoCode = "yes"
lookup generateGoRunCode = "no"
lookup generateSqlOverview = "no"

lookup dbhgetter		= "config.GetDBH"
lookup errlogger		= "config.LogErr"

lookup baseurl          = "/htmxapp2"
lookup goServerPort     = "8082" 

import func ( 
	"database/sql"
	"config" 
	"bytes" 
	"errors"
	"fmt"
	"os"
	"html/template"
)

import test ( 
	"config" 
	"fmt" 
	"testing" 
	"os"
	"html/template"
)
*/»


«/* 
	---------------------------------------------------------- 
	--- START OF TEMPLATE ------------------------------------ 
	---------------------------------------------------------- 
*/»


«if (eq .Lookup.generateGoCode "yes")»
«/* ------ STRUCT & FUNC ------ */»
//FILE: «.Directory»«.Name»_generated_func.go
package «.Package» 
import ( 
«range .ImportList.func»	  «.»
«end»«if .Flag.webservice»    "net/http"
    "encoding/json"«end»
)
«if .NullTypeSet»
import . "database/sql" 
«end»

func justToPleaseTheCompiler_generated() { 
    fmt.Fprintf(os.Stderr,"justToPleaseTheCompiler: %v", errors.New("justToPleaseTheCompiler"))
} 

«/* ------ WEBSERVICE register the functions ------ */»
«if .Flag.webservice»
func RegisterWebServices(mux *http.ServeMux,verbosity int) {
«range .FuncList»«if .Flag.webservice»    mux.HandleFunc("«.Lookup.baseurl»/«.Package»/«.Namelc»json", «.Name»WS)
	fmt.Println()
    if (verbosity>1) {
        //fmt.Println("    «.Lookup.baseurl»/«.Package»/«.Namelc»json(«template "tNameOnlyParamList" .AllParamList»)")
		fmt.Println("curl -sS -X POST -d«ccTrimBlankLine .PayloadRunWS» http://localhost:«.Lookup.goServerPort»«.Lookup.baseurl»/«.Package»/«.Namelc»json")
        //fmt.Println()
    }
«end»
«if .ApplyFlag»    mux.HandleFunc("«.Lookup.baseurl»/«.Package»/«.Namelc»", «.Name»Apply)
    if (verbosity>1) {
        //fmt.Println("    «.Lookup.baseurl»/«.Package»/«.Namelc»(«template "tNameOnlyParamList" .AllParamList»)")
		fmt.Println("curl -sS -X POST -d«ccTrimBlankLine .PayloadRunWS» http://localhost:«.Lookup.goServerPort»«.Lookup.baseurl»/«.Package»/«.Namelc»")
        //fmt.Println()
    }
«end»
«end»
}

func applyReturnError(w http.ResponseWriter, r *http.Request, applyError string, err error) {
	var err2 error
    w.Header().Set("Content-Type", "text/html")
    templ := template.New("T")
	templ, err2 = templ.Parse(applyError)
    if err2!=nil {
        «.Lookup.errlogger»(err2, "Parsing applyerror template: %v")
    }

    err2 = templ.Execute(w, fmt.Sprintf("%v",err)) 
    if err2!=nil {
        «.Lookup.errlogger»(err2, "Executing applyerror template: %v")
    }
    fmt.Fprintln(w)
}
«end»


«template "tBeanList" .BeanList»

«template "tFuncList" .FuncList»
«end» 

«/* ------ TEST ------ */»
«if (eq .Lookup.generateGoCode "yes")»
«if .GenerateRunCodeFlag» 
//FILE: «.Directory»«.Name»_generated_test.go
package «.Package» 

import ( 
«range .ImportList.test»	  «.» 
«end») 

«template "tTestCode" .» 
«end»
«end»


«if .GenerateRunCodeFlag» 
«if (eq .Lookup.generateGoRunCode "yes")»
//FILE: executable/«.Package»_run/main_generated.go
«template "tGoRunExecutableCode" .»  
//FILE: «.Directory»«.Name»_generated_run.go
«template "tGoRunCode" .»	 «/* not to be confused with "tTestCode" */»
«end»
«end»


«/* ------ HTTP RUN (not Test!) ------ */»
«if (or .GenerateRunCodeFlag .GenerateRunWSCodeFlag)»
//FILE: executable/«.Package»_srv/main_generated.go
«template "tHttpServerGoRunExecutableCode" .»
«end»



«if (eq .Lookup.generateSqlOverview "yes")»
«/* ------ SQL ------ */»
//FILE: «.Directory»«.Name»_all.sql 
«template "tSqlList" .FuncList»
«end»

«/* -------------------------------------------------------------------------------------------------- 
    --- WATERSHED: as of here only definitions                                ------------------------ 
    -------------------------------------------------------------------------------------------------- */»

«define "tBeanList"»«range .»
type «.Name»	struct { «template "tBeanMemberList" .BeanMemberList»
}

func (_bean «.Name») String() string {
		return fmt.Sprintf("{«range .BeanMemberList»%v«ccSeparator2Space .S»«end»}",
		«range .BeanMemberList»_bean.«template "tTypeBasedTranslator" .»«.S» «end») 
}
«end»«end»


«define "tTypeBasedTranslator"»«if (eq .T "time.Time")»«.N».Format("2006-01-02")«else»«.N»«end»«end»


«define "tBeanMemberList"»«range .»
	«.N» «.T»«end»«end»



«define "tSqlList"» 
«range .»
«if .PayloadSql»
	 -- «.Name»
«range .PayloadSql» «.» 
«end» 
«end» 
«end»«end»


«define "tHttpServerGoRunExecutableCode"»
package main

import (
    "fmt"
    "log"
    "net/http"
    "time"
    thepackage "«.PackageCompletePath»"
)

func main() {

    mux := http.NewServeMux()
    thepackage.RegisterWebServices(mux, 2)

    mux.Handle("/static/",
        http.StripPrefix("/static/",
            http.FileServer(http.Dir("./static"))))

    fmt.Println("Serving")

    srv := &http.Server{
        Addr:         ":«.Lookup.goServerPort»",
        Handler:      mux,
        ReadTimeout:  5 * time.Second,
        WriteTimeout: 10 * time.Second,
    }
    log.Println(srv.ListenAndServe())
}
«end»


«/* --- Golang METHODS ---------------------------------------------- */»
«define "tFuncList"»

«range .»
	«if .BatchParamFlag»«template "tBatchInsertUpdate" .» 
	«else» «if .ReturnsVoidFlag» «template "tFuncReturnsVoid" .» 
		   «else» «template "tFuncReturnsNonVoid" .» «end»
	«end» 
«end»
«end»


«define "tParamList"»«range .»«.N» «.T»«.S» «end»«end»

«define "tNameOnlyParamList"»«range .»«.N»«.S»«end»«end»

«define "tFuncReturnsVoid"»
func «.Name»(«template "tParamList" .AllParamList») (err error) { 
«if .PayloadGoFlag»«.PayloadGo»«else»
	// WZ01: tFuncReturnsVoid
	var dbh *sql.DB
	dbh,err= «.Lookup.dbhgetter»(«template "tNameOnlyParamList" .DbidParamList»)
	if err!=nil {
		«.Lookup.errlogger»( err, "«.Name»() : «.Lookup.dbhgetter»")
		return err
	}

	var buf bytes.Buffer
	«if .HasInjectionParamFlag»«range .PayloadSqlInjected»buf.WriteString(«.») 
	«end»
	«else»«range .PayloadSql»buf.WriteString("«.»") 
	«end»«end»
	stmt, err := dbh.Prepare( buf.String() ) 
	if err!=nil {
		«.Lookup.errlogger»(err,"«.Name»() : dbh.Prepare()")
		return err
	}
	defer stmt.Close()
   
	_, err = stmt.Exec(«template "tNameOnlyParamList" .QueryParamList»)
	if err!=nil {
		«.Lookup.errlogger»(err,"«.Name»() : stmt.Execute()")
		return err
	}
	return nil	
«end»
}

«if .Flag.json»
func «.Name»Json(«template "tParamList" .AllParamList») (err error ) { 
	// WZ01JSON: tFuncReturnsVoid
	err:= «.Name»(«template "tNameOnlyParamList" .AllParamList») 
	if err!=nil {
		«.Lookup.errlogger»(err,"«.Name»Json() : calling original func")
		return 
	}
	return 
} 
«end»


«if .Flag.webservice»
func «.Name»WS(w http.ResponseWriter, r *http.Request) {
	// WZ01WS: tWSFuncReturnsVoid
    var err error

    fmt.Println("«.Namelc»") // TODO: DROPME

    «template "tRequestParamConversion" .AllParamList»

    err=  «.Name»( «range .AllParamList»«.N»«.S»«end»)
    if err != nil {
        «.Lookup.errlogger»(err,"«.Name»WS(..) calling  «.Name»(..) ")
        http.Error(w,  err.Error(), http.StatusInternalServerError)
        return
    }
}
«end»
«end»




«define "tBatchInsertUpdate"»
func «.Name»(«template "tParamList" .AllParamList») (err error) { 
«if .PayloadGoFlag»«.PayloadGo»«else»
	// SE-02: tBatchInsertUpdate
	var dbh *sql.DB
	dbh,err= «.Lookup.dbhgetter»(«template "tNameOnlyParamList" .DbidParamList»)
	if err!=nil {
		«.Lookup.errlogger»( err, "«.Name»() : «.Lookup.dbhgetter»")
		return err
	}

	// START TRANSACTION 
	trx, err := dbh.Begin()
	if err!=nil {
		«.Lookup.errlogger»( err, "«.Name»() : Begin transaction")
		return err
	}

	var buf bytes.Buffer
	
	«if .HasInjectionParamFlag»
		// BATCH_TYPE_1: injection of []string
	for _,_«.BatchParamName» :=range( «.BatchParamName» ) { 
		«range .PayloadSqlInjected»buf.WriteString(«.») 
		«end»
		_, err := trx.Exec( buf.String() )  // execute on transaction
		if err!=nil {
			trx.Rollback() // ROLLBACK!
			«.Lookup.errlogger»(err,"«.Name»() : trx.Exec()")
			return err
		}
		buf.Reset()
	}
	    // end of BATCH_TYPE_1
	«else»
		// BATCH_TYPE_2: plain (non-injection) batchparam	
	«range .PayloadSql»buf.WriteString("«.»") 
	«end»
	stmt, err := trx.Prepare( buf.String() )  // Prepare on Transaction
	if err!=nil {
		«.Lookup.errlogger»(err,"«.Name»() : trx.Prepare()")
		return err
	}
	defer stmt.Close()

	for _,_pe :=range( «.BatchParamName» ) { 
		_,err = stmt.Exec(«range .BatchParamBean.BeanMemberList» _pe.«.N»«.S»«end» ) 

		if err!=nil {
			trx.Rollback() // ROLLBACK!
			«.Lookup.errlogger»(err,"«.Name»() : stmt.Execute()")
			return err
		}

		// TODO add: batch counter, and do intermediate commits
	}
		// end of BATCH_TYPE_2
	«end» 

	err=trx.Commit() 
	if err!=nil {
		«.Lookup.errlogger»(err,"«.Name»() : trx.Commit()")
		return err
	}

	return nil	
«end»
}
«end»


«define "tFuncReturnsNonVoid"»
  «if .ReturnsMapOfListsFlag» «template "tFuncReturnsMapOfLists" .»«else» 
	«if .ReturnsMapFlag» «template "tFuncReturnsMap" .»«else» 
	  «if .ReturnsListFlag» «template "tFuncReturnsList" .»«else» 
		«if .ReturnsChanFlag» «template "tFuncReturnsChan" .»«else» 
		  «template "tFuncReturnsSingle" .»
		«end»
	  «end»
	«end»
  «end»
«end»


«define "tFuncReturnsSingle"»
func «.Name»(«template "tParamList" .AllParamList») ( _result «.ReturnType», err error) { 
«if .PayloadGoFlag»«.PayloadGo»«else»
	// WZ03: tFuncReturnsSingle
	var dbh *sql.DB
	dbh,err= «.Lookup.dbhgetter»(«template "tNameOnlyParamList" .DbidParamList»)
	if err!=nil {
		«.Lookup.errlogger»( err, "«.Name»() : «.Lookup.dbhgetter»()")
		return 
	}
	
	var buf bytes.Buffer
	«if .HasInjectionParamFlag»«range .PayloadSqlInjected»buf.WriteString(«.») 
	«end»
	«else»«range .PayloadSql»buf.WriteString("«.»") 
	«end»«end»
	stmt, err := dbh.Prepare( buf.String() ) 
	if err!=nil {
		«.Lookup.errlogger»(err,"«.Name»() : dbh.Prepare()")
		return 
	}
	defer stmt.Close()
   
	_rows, err := stmt.Query(«template "tNameOnlyParamList" .QueryParamList»)
	if err!=nil {
		«.Lookup.errlogger»(err,"«.Name»() : stmt.Execute()")
		return 
	}

	if _rows.Next() {
		«if .ReturnTypeIsBuiltinGoType» 
		err = _rows.Scan(&_result) 
		«else» 
		err = _rows.Scan(«range .ReturnBean.BeanMemberList» &_result.«.N»«.S»«end» ) 
		«end»
		if err!=nil {
			«.Lookup.errlogger»(err,"«.Name»() : Result.Scan()")
			return 
		}
	} else { 
		err=errors.New("No result returned") 
	}
	if _rows.Next() {
		// are there more then 1 rows? -> complain!
		err=errors.New("More than 1 result returned") 
	}
	return
«end»
}
«if (or .Flag.json .Flag.webservice)»
func «.Name»Json(«template "tParamList" .AllParamList») ( _result string, err error ) { 
	// WZ03JSON: tFuncReturnsSingle 
		_innerResult,err:= «.Name»(«template "tNameOnlyParamList" .AllParamList») 
	if err!=nil {
		«.Lookup.errlogger»(err,"«.Name»Json() : calling original func")
		return 
	}
  
	_resultBytes,err:=json.Marshal(_innerResult)
	if err!=nil {
		«.Lookup.errlogger»(err,"«.Name»() : marshalling json")
		return 
	}
	_result=string(_resultBytes) 
	return 
} 
«end»

«if .Flag.webservice»
func «.Name»WS(w http.ResponseWriter, r *http.Request) {
	// WZ03WS: tFuncReturnsSingle 
    fmt.Println("«.Namelc»") // TODO: DROPME
    «template "tRequestParamConversion" .AllParamList»

    _result,err:=  «.Name»Json( «range .AllParamList»«.N»«.S»«end»)
    if err != nil {
        «.Lookup.errlogger»(err,"«.Name»WS(..) calling  «.Name»Json(..) ")
        http.Error(w,  err.Error(), http.StatusInternalServerError)
        return
    }
    w.Header().Set("Content-Type", "application/json")
    w.Write([]byte(_result))
}
«end»

«if .ApplyFlag»
func «.Name»Apply(w http.ResponseWriter, r *http.Request) {
	// WZ03APPLY: tFuncReturnsSingle
	fmt.Println("Calling «.Name»(«template "tNameOnlyParamList" .AllParamList»)")
    «template "tRequestParamConversion" .AllParamList»
	_result,err:= «.Name»(«template "tNameOnlyParamList" .AllParamList») 
    if err != nil {
        «.Lookup.errlogger»(err,"«.Name»Apply(..) calling  «.Name»(..) ")
		«if .ApplyErrorFlag»
			applyReturnError(w,r,`«.ApplyError»`, err) 
		«else»
        http.Error(w,  err.Error(), http.StatusInternalServerError)
		«end»
        return
    }

    w.Header().Set("Content-Type", "text/html")
	templ := template.New("T")
    templ, err = templ.Parse(`«.ApplyBody»`) 
    if err!=nil {
		«.Lookup.errlogger»(err, "Executing Test«.Name» %v")
        http.Error(w,  err.Error(), http.StatusInternalServerError)
	}

	err = templ.Execute(w, _result)
    if err!=nil {
		«.Lookup.errlogger»(err, "Executing Test«.Name» %v")
        http.Error(w,  err.Error(), http.StatusInternalServerError)
	}
	fmt.Fprintln(w)
}
«end»

«end»


«define "tFuncReturnsList"»
func «.Name»(«template "tParamList" .AllParamList») ( _result «.ReturnType», err error) { 
«if .PayloadGoFlag»«.PayloadGo»«else»
	// WZ04: tFuncReturnsList
	var dbh *sql.DB
	dbh,err= «.Lookup.dbhgetter»(«template "tNameOnlyParamList" .DbidParamList»)
	if err!=nil {
		«.Lookup.errlogger»( err, "«.Name»() : «.Lookup.dbhgetter»()")
		return 
	}
	
	var buf bytes.Buffer
	«if .HasInjectionParamFlag»«range .PayloadSqlInjected»buf.WriteString(«.») 
	«end»
	«else»«range .PayloadSql»buf.WriteString("«.»") 
	«end»«end»
	stmt, err := dbh.Prepare( buf.String() ) 
	if err!=nil {
		«.Lookup.errlogger»(err,"«.Name»() : dbh.Prepare()")
		return 
	}
	defer stmt.Close()
   
	_rows, err := stmt.Query(«template "tNameOnlyParamList" .QueryParamList»)
	if err!=nil {
		«.Lookup.errlogger»(err,"«.Name»() : stmt.Execute()")
		return 
	}

	var _re «.ReturnListType» 
	for _rows.Next() {
		«if .ReturnTypeIsBuiltinGoType» 
		err = _rows.Scan(&_re) 
		«else» 
		err = _rows.Scan(«range .ReturnBean.BeanMemberList» &_re.«.N»«.S»«end» ) 
		«end»
		if err!=nil {
			«.Lookup.errlogger»(err,"«.Name»() : Result.Scan()")
			return 
		}
		// add the element to the collection
		_result=append(_result,_re) 
	}
	return
«end»
}

«if (or .Flag.json .Flag.webservice)»
func «.Name»Json(«template "tParamList" .AllParamList») ( _result string, err error ) { 
	// WZ04JSON: tFuncReturnsList
	_innerResult,err:= «.Name»(«template "tNameOnlyParamList" .AllParamList») 
	if err!=nil {
		«.Lookup.errlogger»(err,"«.Name»Json() : calling original func")
		return 
	}
  
	_resultBytes,err:=json.Marshal(_innerResult)
	if err!=nil {
		«.Lookup.errlogger»(err,"«.Name»() : marshalling json")
		return 
	}
	_result=string(_resultBytes) 
	return 
} 
«end»

«if .Flag.webservice»
func «.Name»WS(w http.ResponseWriter, r *http.Request) {
    fmt.Println("«.Namelc»") // TODO: DROPME
	// WZ04WS: tFuncReturnsList
    «template "tRequestParamConversion" .AllParamList»

    _result,err:=  «.Name»Json( «range .AllParamList»«.N»«.S»«end»)
    if err != nil {
        «.Lookup.errlogger»(err,"«.Name»WS(..) calling  «.Name»Json(..) ")
        http.Error(w,  err.Error(), http.StatusInternalServerError)
        return
    }
    w.Header().Set("Content-Type", "application/json")
    w.Write([]byte(_result))
}
«end»

«if .ApplyFlag»
func «.Name»Apply(w http.ResponseWriter, r *http.Request) {
	// WZ04APPLY: tFuncReturnsList
	fmt.Println("Calling «.Name»(«template "tNameOnlyParamList" .AllParamList»)")
    «template "tRequestParamConversion" .AllParamList»
	_result,err:= «.Name»(«template "tNameOnlyParamList" .AllParamList») 
    if err != nil {
        «.Lookup.errlogger»(err,"«.Name»Apply(..) calling  «.Name»(..) ")
		«if .ApplyErrorFlag»
			applyReturnError(w,r,`«.ApplyError»`, err) 
		«else»
        http.Error(w,  err.Error(), http.StatusInternalServerError)
		«end»
        return
    }

    w.Header().Set("Content-Type", "text/html")
	templ := template.New("T")
    templ, err = templ.Parse(`«.ApplyBody»`) 
    if err!=nil {
		«.Lookup.errlogger»(err, "Executing Test«.Name» %v")
        http.Error(w,  err.Error(), http.StatusInternalServerError)
	}

	//fmt.Printf("\n%v\n", _result) 
	err = templ.Execute(w, _result)
    if err!=nil {
		«.Lookup.errlogger»(err, "Executing Test«.Name» %v")
        http.Error(w,  err.Error(), http.StatusInternalServerError)
	}
	fmt.Fprintln(w)
}
«end»

«end»



«define "tFuncReturnsChan"»
func «.Name»(«template "tParamList" .AllParamList») ( _outchan «.ReturnType», err error) { 
«if .PayloadGoFlag»«.PayloadGo»«else»
	// SE-05: tFuncReturnsChan
	var dbh *sql.DB
	dbh,err= «.Lookup.dbhgetter»(«template "tNameOnlyParamList" .DbidParamList»)
	if err!=nil {
		«.Lookup.errlogger»( err, "«.Name»() : «.Lookup.dbhgetter»()")
		return 
	}
	
	var buf bytes.Buffer
	«if .HasInjectionParamFlag»«range .PayloadSqlInjected»buf.WriteString(«.») 
	«end»
	«else»«range .PayloadSql»buf.WriteString("«.»") 
	«end»«end»
	stmt, err := dbh.Prepare( buf.String() ) 
	if err!=nil {
		«.Lookup.errlogger»(err,"«.Name»() : dbh.Prepare()")
		return 
	}
	defer stmt.Close()
   
	_rows, err := stmt.Query(«template "tNameOnlyParamList" .QueryParamList»)
	if err!=nil {
		«.Lookup.errlogger»(err,"«.Name»() : stmt.Execute()")
		return 
	}

	_outchan=make(chan «.ReturnChanType»)
	go func() {
		var _re «.ReturnChanType» 
		for _rows.Next() {
			«if .ReturnTypeIsBuiltinGoType» 
			err = _rows.Scan(&_re) 
			«else» 
			err = _rows.Scan(«range .ReturnBean.BeanMemberList» &_re.«.N»«.S»«end» ) 
			«end»
			if err!=nil {
				«.Lookup.errlogger»(err,"«.Name»() : Result.Scan()")
				break
			}
			_outchan <- _re
		}
		close(_outchan)
	}()
	return 
«end»
}
«if .Flag.json»
	//		note aboute .Flag.json
	// SE-05b: tFuncReturnsChan
	//		NOT YET IMPLEMENTED!
«end»
«end»


«define "tFuncReturnsMap"»
func «.Name»(«template "tParamList" .AllParamList») ( _result «.ReturnType», err error) { 
«if .PayloadGoFlag»«.PayloadGo»«else»
	// WZ06: tFuncReturnsMap
	var dbh *sql.DB
	dbh,err= «.Lookup.dbhgetter»(«template "tNameOnlyParamList" .DbidParamList»)
	if err!=nil {
		«.Lookup.errlogger»( err, "«.Name»() : «.Lookup.dbhgetter»()")
		return 
	}
	
	var buf bytes.Buffer
	«if .HasInjectionParamFlag»«range .PayloadSqlInjected»buf.WriteString(«.») 
	«end»
	«else»«range .PayloadSql»buf.WriteString("«.»") 
	«end»«end»
	stmt, err := dbh.Prepare( buf.String() ) 
	if err!=nil {
		«.Lookup.errlogger»(err,"«.Name»() : dbh.Prepare()")
		return 
	}
	defer stmt.Close()

	_rows, err := stmt.Query(«template "tNameOnlyParamList" .QueryParamList»)
	if err!=nil {
		«.Lookup.errlogger»(err,"«.Name»() : stmt.Execute()")
		return 
	}

	var _rk «.ReturnMapKeyType» 
	var _rv «.ReturnMapValueType» 
	_result=make( «.ReturnType»)

	for _rows.Next() {
	«if .ReturnTypeIsBuiltinGoType» 
		err = _rows.Scan(&_rk, &_rv) 
	«else» 
		err = _rows.Scan(&_rk, «range .ReturnBean.BeanMemberList» &_rv.«.N»«.S»«end» ) 
	«end»
		if err!=nil {
			«.Lookup.errlogger»(err,"«.Name»() : Result.Scan()")
			return 
		}
		_result[_rk]=_rv  // add the element to the map
	}
	return
«end»
}
«if (or .Flag.json .Flag.webservice)»
func «.Name»Json(«template "tParamList" .AllParamList») ( _result string, err error ) { 
	// WZ06JSON: tFuncReturnsMap
	_innerResult,err:= «.Name»(«template "tNameOnlyParamList" .AllParamList») 
	if err!=nil {
		«.Lookup.errlogger»(err,"«.Name»Json() : calling original func")
		return 
	}
  
	_resultBytes,err:=json.Marshal(_innerResult)
	if err!=nil {
		«.Lookup.errlogger»(err,"«.Name»() : marshalling json")
		return 
	}
	_result=string(_resultBytes) 
	return 
} 
«end»

«if .Flag.webservice»
func «.Name»WS(w http.ResponseWriter, r *http.Request) {
    fmt.Println("«.Namelc»") // TODO: DROPME
	// WZ06WS: tFuncReturnsMap
    «template "tRequestParamConversion" .AllParamList»

    _result,err:=  «.Name»Json( «range .AllParamList»«.N»«.S»«end»)
    if err != nil {
        «.Lookup.errlogger»(err,"«.Name»WS(..) calling  «.Name»Json(..) ")
        http.Error(w,  err.Error(), http.StatusInternalServerError)
        return
    }
    w.Header().Set("Content-Type", "application/json")
    w.Write([]byte(_result))
}
«end»

«if .ApplyFlag»
func «.Name»Apply(w http.ResponseWriter, r *http.Request) {
	// WZ06APPLY: tFuncReturnsMap
	fmt.Println("Calling «.Name»(«template "tNameOnlyParamList" .AllParamList»)")
    «template "tRequestParamConversion" .AllParamList»
	_result,err:= «.Name»(«template "tNameOnlyParamList" .AllParamList») 
    if err != nil {
        «.Lookup.errlogger»(err,"«.Name»Apply(..) calling  «.Name»(..) ")
		«if .ApplyErrorFlag»
			applyReturnError(w,r,`«.ApplyError»`, err) 
		«else»
        http.Error(w,  err.Error(), http.StatusInternalServerError)
		«end»
        return
    }

    w.Header().Set("Content-Type", "text/html")
	templ := template.New("T")
    templ, err = templ.Parse(`«.ApplyBody»`) 
    if err!=nil {
		«.Lookup.errlogger»(err, "Executing Test«.Name» %v")
        http.Error(w,  err.Error(), http.StatusInternalServerError)
	}

	//fmt.Printf("\n%v\n", _result) 
	err = templ.Execute(w, _result)
    if err!=nil {
		«.Lookup.errlogger»(err, "Executing Test«.Name» %v")
        http.Error(w,  err.Error(), http.StatusInternalServerError)
	}
	fmt.Fprintln(w)
}
«end»
«end»



«define "tFuncReturnsMapOfLists"»
func «.Name»(«template "tParamList" .AllParamList») ( _result «.ReturnType», err error) { 
«if .PayloadGoFlag»«.PayloadGo»«else»
	// WZ07: tFuncReturnsMapOfLists
	var dbh *sql.DB
	dbh,err= «.Lookup.dbhgetter»(«template "tNameOnlyParamList" .DbidParamList»)
	if err!=nil {
		«.Lookup.errlogger»( err, "«.Name»() : «.Lookup.dbhgetter»()")
		return 
	}
	
	var buf bytes.Buffer
	«if .HasInjectionParamFlag»«range .PayloadSqlInjected»buf.WriteString(«.») 
	«end»
	«else»«range .PayloadSql»buf.WriteString("«.»") 
	«end»«end»
	stmt, err := dbh.Prepare( buf.String() ) 
	if err!=nil {
		«.Lookup.errlogger»(err,"«.Name»() : dbh.Prepare()")
		return 
	}
	defer stmt.Close()

	_rows, err := stmt.Query(«template "tNameOnlyParamList" .QueryParamList»)
	if err!=nil {
		«.Lookup.errlogger»(err,"«.Name»() : stmt.Execute()")
		return 
	}

	var _rk «.ReturnMapKeyType» 
	var _rv «.ReturnMapValueType» 
	_result=make( «.ReturnType»)

	for _rows.Next() {
	«if .ReturnTypeIsBuiltinGoType» 
		err = _rows.Scan(&_rk, &_rv) 
	«else» 
		err = _rows.Scan(&_rk, «range .ReturnBean.BeanMemberList» &_rv.«.N»«.S»«end» ) 
	«end»
		if err!=nil {
			«.Lookup.errlogger»(err,"«.Name»() : Result.Scan()")
			return 
		}
		// add the element to the list that is stored in the map
		_,exists:= _result[_rk]
		if (!exists) {
			_result[_rk]=make( []«.ReturnMapValueType», 0, 1 )
		}
		_result[_rk]=append(_result[_rk],_rv)  
	}
	return
«end»	   
}
«if (or .Flag.json .Flag.webservice)»
func «.Name»Json(«template "tParamList" .AllParamList») ( _result string, err error ) { 
	// WZ07JSON: tFuncReturnsMapOfLists
	_innerResult,err:= «.Name»(«template "tNameOnlyParamList" .AllParamList») 
	if err!=nil {
		«.Lookup.errlogger»(err,"«.Name»Json() : calling original func")
		return 
	}
  
	_resultBytes,err:=json.Marshal(_innerResult)
	if err!=nil {
		«.Lookup.errlogger»(err,"«.Name»() : marshalling json")
		return 
	}
	_result=string(_resultBytes) 
	return 
} 
«end»

«if .Flag.webservice»
func «.Name»WS(w http.ResponseWriter, r *http.Request) {
    fmt.Println("«.Namelc»") // TODO: DROPME
	// WZ07WS: tFuncReturnsMapOfLists
    «template "tRequestParamConversion" .AllParamList»

    _result,err:=  «.Name»Json( «range .AllParamList»«.N»«.S»«end»)
    if err != nil {
        «.Lookup.errlogger»(err,"«.Name»WS(..) calling  «.Name»Json(..) ")
        http.Error(w,  err.Error(), http.StatusInternalServerError)
        return
    }
    w.Header().Set("Content-Type", "application/json")
    w.Write([]byte(_result))
}
«end»

«if .ApplyFlag»
func «.Name»Apply(w http.ResponseWriter, r *http.Request) {
	// WZ07APPLY: tFuncReturnsMapOfLists
	fmt.Println("Calling «.Name»(«template "tNameOnlyParamList" .AllParamList»)")
    «template "tRequestParamConversion" .AllParamList»
	_result,err:= «.Name»(«template "tNameOnlyParamList" .AllParamList») 
    if err != nil {
        «.Lookup.errlogger»(err,"«.Name»Apply(..) calling  «.Name»(..) ")
		«if .ApplyErrorFlag»
			applyReturnError(w,r,`«.ApplyError»`, err) 
		«else»
        http.Error(w,  err.Error(), http.StatusInternalServerError)
		«end»
        return
    }

    w.Header().Set("Content-Type", "text/html")
	templ := template.New("T")
    templ, err = templ.Parse(`«.ApplyBody»`) 
    if err!=nil {
		«.Lookup.errlogger»(err, "Executing Test«.Name» %v")
        http.Error(w,  err.Error(), http.StatusInternalServerError)
	}

	//fmt.Printf("\n%v\n", _result) 
	err = templ.Execute(w, _result)
    if err!=nil {
		«.Lookup.errlogger»(err, "Executing Test«.Name» %v")
        http.Error(w,  err.Error(), http.StatusInternalServerError)
	}
	fmt.Fprintln(w)
}
«end»
«end»



«define "tRequestParamConversion"»/* --- Retrieve param from request and optionally convert --- */«range .»
    «if eq .T "int"»// convert string to int
    «.N», err  := strconv.Atoi(r.FormValue("«.N»"))
    if err != nil {
        http.Error(w,  err.Error(), http.StatusInternalServerError)
        return
    }
«end»«if eq .T "int64"»// convert string to int64
    «.N», err  := strconv.ParseInt(r.FormValue("«.N»"),10, 64)
    if err != nil {
        http.Error(w,  err.Error(), http.StatusInternalServerError)
        return
    }
«end»«if eq .T "float64"»// convert string to float64
    «.N», err  := strconv.ParseFloat(r.FormValue("«.N»"),64)
    if err != nil {
        http.Error(w,  err.Error(), http.StatusInternalServerError)
        return
    }
«end»«if eq .T "string"»// string : retrieve without conversion
    «.N» := r.FormValue("«.N»")«end»«end»
«end»

«define "tUrlParamConversion"»«if eq .T "int"» strconv.FormatInt(int64(«.N»), 10)«end»«if eq .T "int32"» strconv.FormatInt(int64(«.N»), 10)«end»«if eq .T "int64"» strconv.FormatInt(«.N», 10)«end»«if eq .T "string"» «.N»«end»«end»



«/* --- Test Code funcs ---------------------------------------------- 
*/»
«define "tTestCode"» 

«range .FuncList»«if .GenerateRunCodeFlag»
func Test«.Name»(t *testing.T) {
	// SE-08: tTestCode
	«.PayloadRun»
	«if .ReturnsVoidFlag» 
	err:= «.Name»(«template "tNameOnlyParamList" .AllParamList»)
	if err!=nil {
		«.Lookup.errlogger»(err,"Executing Test«.Name»")
	}
	«else» 
	_result, err:= «.Name»(«template "tNameOnlyParamList" .AllParamList»)
	if err!=nil {
		«.Lookup.errlogger»(err,"Executing Test«.Name»")
	} else {
		«if .ApplyFlag» 
		«if .ReturnsListFlag» 
		// SE-08-APPLY-A: tTestCode 
		templ := template.New("T")
        templ, err := templ.Parse(`«.ApplyBody»`) 
		err = templ.Execute(os.Stdout, _result)
		if err!=nil {
			t.Errorf("Executing Test«.Name» %v",err)
		}
		«else»
		// SE-08-APPLY-B: tTestCode 
		fmt.Printf("%v\n",_result)
		«end»«end»
	}
	«end»
	«if .NoPayloadRunPSFlag»
	if err!=nil {
		t.Errorf("Executing Test«.Name» %v",err)
	}
	«else» 
	«.PayloadRunPS»
	«end» 
}	 
«end»

«if .Flag.json»

func Test«.Name»Json(t *testing.T) {
	// SE-08b: tTestCode
	«.PayloadRun»
	«if .ReturnsVoidFlag» 
	err:= «.Name»Json(«template "tNameOnlyParamList" .AllParamList»)
	if err!=nil {
		«.Lookup.errlogger»(err,"Executing Test«.Name»Json")
	}
	«else» 
	_result, err:= «.Name»Json(«template "tNameOnlyParamList" .AllParamList»)
	if err!=nil {
		«.Lookup.errlogger»(err,"Executing Test«.Name»Json")
	} else {
		«if .ReturnsChanFlag» 
		// note: the result is a <-chan !
		for _rv:= range _result { 
			fmt.Printf("%v\n",_rv)
		}
		«else»
		fmt.Printf("%v\n",_result)
		«end»
	}
	«end»
	«if .NoPayloadRunPSFlag»
	// By default, when no payloadrunPS is given, register a test failure on error
	if err!=nil {
		t.Errorf("Executing Test«.Name»Json %v",err)
	}
	«else» 
	«.PayloadRunPS»
	«end» 
}	 
«end»«end»«end»


«/* --- Go Executable Code ---------------------------------------------- */»
«define "tGoRunExecutableCode"»  
package main 

import ( 
	"fmt" 
	servicepackage "«.PackageCompletePath»"
	) 

func main() { 
	/*fmt.Println(">>> START GO TEST");*/«range .FuncList»«if .GenerateRunCodeFlag»
	fmt.Println()
	fmt.Print("«.Package».«.Name»\t")
	servicepackage.Run«.Name»();«end»«end»
	/*fmt.Println(">>> END GO TEST");*/
}
«end»

«/* --- Go Run Code ---------------------------------------------- */»
«define "tGoRunCode"» 
package «.Package»
import ( 
	"fmt" 
	"os"
	"errors"
«range .ImportList.run»	 «.»
«end»)

func justToPleaseTheCompiler_run() { 
    fmt.Fprintf(os.Stderr,"justToPleaseTheCompiler: %v", errors.New("justToPleaseTheCompiler"))
} 

«range .FuncList»«if .GenerateRunCodeFlag»
func Run«.Name»() {
«ccTrimBlankLine .PayloadRun»
	«if .ReturnsVoidFlag» // SE-GORUN-01a
	err:= «.Name»(«template "tNameOnlyParamList" .AllParamList»)
	if err!=nil {
		//fmt.Fprintf(os.Stderr,"Error executing run«.Name»: %s\n",  err.Error())
		fmt.Printf(err.Error())
	}
	«else»	// SE-GORUN-01b
	_result, err:= «.Name»(«template "tNameOnlyParamList" .AllParamList»)
	if err!=nil {
		//fmt.Fprintf(os.Stderr,"Error executing run«.Name»: %s\n",  err.Error())
		fmt.Printf(err.Error())
		return
	} 
	«end»
	«if .NoPayloadRunPSFlag»/* NoPayloadRunPSFlag */ 
	«if not .ReturnsVoidFlag»
	//if (1==2) { fmt.Printf("Dummy code to avoid compiler complaints: %v", _result); }
	«if .ReturnsListFlag»
	fmt.Printf("list_contains_%d_elements|", len(_result));		// SE-GORUN-02
	for i,v:=range(_result) {
		fmt.Printf("%d:%v|", i, v)
	}
	«else»«if .ReturnsMapFlag»
	fmt.Printf("map_contains_%v_elements|", len(_result))		// SE-GORUN-03
    for k,v:=range( _result ) {
		// k: type «.ReturnMapKeyType» with formatSpec «translateFormatSpec .ReturnMapKeyType»
		// v: type «.ReturnMapValueType»  with formatSpec «translateFormatSpec .ReturnMapValueType»
		fmt.Printf("«translateFormatSpec .ReturnMapKeyType»->«translateFormatSpec .ReturnMapValueType»|", k,v)
    }
	«else»«if .ReturnsMapOfListsFlag»
	fmt.Printf("map_contains_%v_elements|", len(_result))		// SE-GORUN-04
    for k,v:=range( _result ) {
        fmt.Printf("%v->[", k)
        for i,l:=range(v) {
            fmt.Printf("%v:%v,", i,l)
		}
        fmt.Print("]|")
    }
	«else»
	fmt.Printf("%v\n", _result);								// SE-GORUN-05
	«end»«end»«end»
	«else» 
	fmt.Println("Function returns void.");						// SE-GORUN-06
	«end»«else» 
«ccTrimBlankLine .PayloadRunPS»
	«end» 
}	 
«end»

«end»
«end»
