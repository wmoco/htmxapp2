#!/bin/bash

export GOPATH=`pwd`:$HOME/go:$HOME/go_github
export PATH=`pwd`/bin:$HOME/go/bin:$PATH

# copy all the html files of the src tree to static
(cd src/htmxapp2; rsync -t -r --progress \
	--exclude="*/*.amg" \
	--exclude="*/*.go" \
	--exclude="*/*.md" \
	* ../../static ) 

# generate go code / build / execute
amgo && go build executable/htmxapp2server && ./htmxapp2server

