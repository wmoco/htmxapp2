# htmxapp2

My second htmx app. First project on GITLAB.

## Build instructions

- install go 
- install amalegeni (version 0.25 or higher) 

~~~shell
curl -s  http://www.wmo.co/amalegenigo/download/amgo64-0.25.tar.xz | tar xvfJ - 
mv amgo /usr/local/bin			#or   mv amgo ~/bin 
~~~

- ensure that both 'go' and 'amgo' are in your PATH 
- install the postgres client library on your linux box: `apt-get install postgresql-client-11 postgresql-client-common`
- get the Postgres driver for `go`: 

~~~shell
go get github.com/lib/pq
~~~

- pull and start the dockerized database: 
~~~shell
docker pull wmoco/htmxapp2db
docker run -p 45432:5432 -d wmoco/htmxapp2db
~~~
- optional: give the db a testrun:
~~~shell
psql -h localhost -p 45432 -d htmxapp2db -U cewcew		# password: cew99cew99 

htmxapp2db=> select * from t_population_projection;
~~~

- clone this repo, and build/run: 
~~~shell
git clone https://gitlab.com/wmoco/htmxapp2
cd htmxapp2
bash ./buildnrun.sh 
~~~
The server should be running now.


- optional: test a service on the command line (in another terminal window): 
~~~shell
curl -sS -X POST -d cc2=GR   http://localhost:8082/htmxapp2/x04/getcountrycitylist

    <table class="detail">
    <tr> <th class="txt" style="width:50%">Name</th>
         <th class="nbr">Lat</th>
         <th class="nbr">Lon</th>
         <th class="nbr">Elevation</th>
    </tr>
    
            <tr>
                <td class="txt">Livádi</td>
                <td class="nbr">40.12625</td>
                <td class="nbr">22.15753</td>
                <td class="nbr">1169</td>
            </tr>
    
            <tr>
                <td class="txt">Metsovo</td>
                <td class="nbr">39.76944</td>
                <td class="nbr">21.18222</td>
                <td class="nbr">1145</td>
            </tr>
    
            <tr>
                <td class="txt">Kerasochóri</td>
                <td class="nbr">39.00556</td>
                <td class="nbr">21.63778</td>
                <td class="nbr">1012</td>
            </tr>
..
..
~~~


- open the web application in your favorite browser: [localhost:8082/htmxapp2/static/htmxapp2.html](http://localhost:8082/htmxapp2/static/htmxapp2.html)


