
# <small>x03:</small>Query producing a map

## Left pane: GetCCMap

The query is similar to previous examples.

~~~ {#mycode .sql}
	select	cceu, 
			to_char(schengen_implementation_date,'YYYY-MM-DD') 
	from	t_eu_country 
	where   schengen_implementation_date is not null 
~~~

Sample result: 

~~~ {#mycode .sql}
 key |   value    
-----+------------
 BE  | 1995-03-26
 DE  | 1995-03-26
 FR  | 1995-03-26
 IT  | 1997-10-26
 LU  | 1995-03-26
..
~~~

The difference now is the return type of the pseudo-go function: it returns a map of string -> string, or key -> value.
 
~~~ {#mycode .go}
func GetCCMap() map[string]string
~~~

The first field of the select will be taken as the key, the second the value. Sounds logical, no? 


In the HTML template you access the map as follows: `{{ .<KEY> }}`

~~~{#mycode .html}
apply [[
	France implemented Schengen on {{ .FR }}, and Poland did so on {{ .PL  }}. 
]]
~~~


## Right pane : GetStartDateMap

The query: 

~~~{#mycode .sql}
	select	to_char(schengen_implementation_date,'YYYY-MM-DD'), 
			string_agg(short_name,', ')   
	from	t_eu_country 
	where	schengen_implementation_date is not null 
	group by to_char(schengen_implementation_date,'YYYY-MM-DD')
~~~

Sample result: 

~~~{#mycode .sql}
 key        | value    
------------+-----------------------------------------------------------------------------------
 2000-01-01 | Ελλάδα
 1997-10-26 | Italia
 1995-03-26 | Belgique/België, Deutschland, France, Luxembourg, Nederland, España, Portugal
 ..
~~~

In this case the key is composed of more than just alphanumeric characters, so you need to access the map differently in the  html template: 

~~~{#mycode .go}
{{ index . "<<KEY>>" }}`
~~~

or as demonstrated by the template here:

~~~{#mycode .html}
apply [[
	<li> the countries that implemented schengen on 2001-03-25 are {{ index . "2001-03-25"}} </li>
	<li> and on 2007-12-21 these countries did so too {{ index . "2007-12-21" }} </li>
]]
~~~


# Source

Everything put together: 

~~~ {#mycode .sql}
func GetCCMap() map[string]string
flag:webservice
<<
	select	cceu, 
			to_char(schengen_implementation_date,'YYYY-MM-DD') 
	from	t_eu_country 
	where   schengen_implementation_date is not null 
>>
runws {{ dummy=1  }}
apply [[
	France implemented Schengen on {{ .FR }}, and Poland did so on {{ .PL  }}. 
]]
applyerror [[
	<div  class="err"> Error: {{ . }} </div> 
]] 



func GetStartDateMap() map[string]string
flag:webservice
<<
	select	to_char(schengen_implementation_date,'YYYY-MM-DD'), 
			string_agg(short_name,', ')   
	from	t_eu_country 
	where	schengen_implementation_date is not null 
	group by to_char(schengen_implementation_date,'YYYY-MM-DD')
>>
runws {{ dummy=1  }}
apply [[
	<li> the countries that implemented schengen on 2001-03-25 are {{ index . "2001-03-25"}} </li>
	<li> and on 2007-12-21 these countries did so too {{ index . "2007-12-21" }} </li>
]]
applyerror [[
	<div  class="err"> Error: {{ . }} </div> 
]] 
~~~
