# <small>x05:</small>  Creating a composite 

Up to now you've seen primitives being returned: a single value, a map, a list of values or struct. All produced by SQL. When we are talking composites, we bring a middle-man into play: a bit of go code that calls other database querying services to assemble a struct like this one : 

~~~{#mysource .go}
type Composite struct {
    NumberFound int
    NumberDisplayed int
    CityLocationList []CityLocation
~~~

This is the go code that assembles it. 

~~~{#mysource .go}
func GetCitySearch(namepat string) Composite
flag:webservice
{{
	_result=Composite{ NumberFound:0, NumberDisplayed:0 }

	_result.NumberFound,err=CountCity(namepat) 
	if (err!=nil) {
		return
	}
	_result.CityLocationList,err=SearchCity(namepat) 
	if (err!=nil) {
		return
	}
	_result.NumberDisplayed=len( _result.CityLocationList ) 
	return
}}
~~~

Note: in amalegeni if you define a function with double curly braces, the payload is expected to be go-code, if you use double less-then/greater-then 'braces' then the payload is expected to be sql. 


~~~{#mysource .go}
func GetCitySearch(..) 
{{
	..
	GO CODE
	..
}}

func CountCity(namepat string) int
<<
	 ..
	 SQL CODE
	 ..
>>
~~~





# Source

## Frontend 

~~~{#mycode .html} 
		<h4> Search for cities <span class="htmx-indicator"> <img src="img/bars.svg"/> Searching...  </span> </h4>
		<p>
			<input class="form-control" type="text"  style="min-width:400px"
			   name="namepat" placeholder="Begin Typing To Search Cities..." 
			   hx-post="/htmxapp2/x05/getcitysearch" 
			   hx-trigger="keyup changed delay:500ms" 
			   hx-target="#search-results" 
			   hx-indicator=".htmx-indicator">
		</p>
		<div id="search-results">
		</div>
~~~


## Backend 

~~~{#mysource .go}
type Composite struct {
    NumberFound int
    NumberDisplayed int
    CityLocationList []CityLocation
}

type CityLocation struct {
    Name string
    Latitude string
    Longitude string
    Elevation int64
	Country string
	Population int64
}


func GetCitySearch(namepat string) Composite
flag:webservice
{{
	_result=Composite{ NumberFound:0, NumberDisplayed:0 }

	_result.NumberFound,err=CountCity(namepat) 
	if (err!=nil) {
		return
	}
	_result.CityLocationList,err=SearchCity(namepat) 
	if (err!=nil) {
		return
	}
	_result.NumberDisplayed=len( _result.CityLocationList ) 
	return
}}
runws {{ namepat=Amste }} 
apply [[
	{{ if eq 0 .NumberFound }}
	No result. 
	{{ else }} 
	<center><small>Displaying {{ .NumberDisplayed }} of {{ .NumberFound }} records found.</small></center> 
	<table class="detail">
    <tr> <th class="txt" style="width:50%">Name (cc)</th>
         <th class="nbr">Lat</th>
         <th class="nbr">Lon</th>
         <th class="nbr">Population</th>
         <th class="nbr">Elevation</th>
    </tr>
	{{range .CityLocationList}}
		<tr>
			<td class="txt" >{{.Name}} ({{.Country}})</td> 
			<td class="nbr">{{.Latitude}}</td>
			<td class="nbr">{{.Longitude}}</td> 
			<td class="nbr">{{.Population}}</td>
			<td class="nbr">{{.Elevation}}</td>
		</tr>
	{{end}}
	</table>
	{{ end }} 
]]
applyerror [[
	<div  class="err"> Error: {{ . }} </div> 
]]


func CountCity(namepat string) int
<<
	 select count(1) 
	 from  t_gn_city 
	 where name like $1||'%'
	 limit 25
>>


func SearchCity(namepat string) []CityLocation
<<
	 select name, latitude, longitude, elevation, country, population
	 from  t_gn_city 
	 where name like $1||'%'
	 order by name
	 limit 25
>>
~~~

