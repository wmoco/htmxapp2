# <small>x01:</small> Query producing a single value

## Front end code

The HTML select component of the front-end looks like this: 

~~~ {#mycode .html}
        <select id="countries" name="cceu" hx-post="/htmxapp2/x01/geteucountryname" hx-target="#rightactionpane">
		  ..
          <option value="BE">Belgique/België</option>
          <option value="DE">Deutschland</option>
          <option value="FR">France</option>
		  ..
		  ..
        </select>   
      </div>    
    ..
~~~

When picking a country from the list, htmx issues a post to */htmxapp2/x01/geteucountryname*

## Backend code 

The code that defines that backend-service, basically boils down to this: 

~~~ {#mycode .html}
func GetEuCountryName(cceu string) string
flag:webservice
<<
    select official_name_en 
	from t_eu_country
	where cceu=$1
>>
apply [[
	<div class="selecttrg"> {{ . }} </div> 
]] 
~~~

For clarity a few things have been left out, please see the detailed breakdown, or have a look 
here: [gitlab.com/../single-hl.amg](https://gitlab.com/wmoco/htmxapp2/-/blob/master/src/htmxapp2/x01/single-hl.amg)





## Detail

For more detail about the backend read: <button hx-post="x01/detail.html" hx-target="#tarp">the detailed breakdown</button>
