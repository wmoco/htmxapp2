
# Single select statement

## Backend code 

This is all of the backend code: sql encapsulated in go, enhanced with html.

~~~ {#mycode .html}
func GetEuCountryName(cceu string) string
flag:webservice
<<
    select  official_name_en 
    from    t_eu_country
    where   cceu=$1
    and     current_date between eu_entry_date and eu_exit_date
>>
runws {{ 'cceu=LU' }}
apply [[
    <div class="selecttrg"> {{ . }} </div>
]]
applyerror [[
    <div  class="selecttrgerr"> Error: {{ . }} </div>
]]
~~~


## Breakdown

### The function declaration in (pseudo) Go

~~~ {#mycode .html}
func GetEuCountryName(cceu string) string
flag:webservice
~~~

I assume you have no problem deducing that this function takes 1 string argument and returns 1 string.

If you look in the generated Go code, you can tell that it actually is returning both a string and error value. So the translation of this function declaration to a Go-lang function is not 100% literal. When you dig deeper into amalegeni you'll see more of this (hint: see db and injection parameters).

When we add the *flag:webservice* to the function declaration, we tell that the generated code will include a webservice that returns the result in JSON. 

### The SQL body 

The body of the function, delimited by << .. >> is this SQL statement: 

~~~ {#mycode .sql}
<<
    select  official_name_en 
    from    t_eu_country
    where   cceu=$1
    and     current_date between eu_entry_date and eu_exit_date
>>
~~~


### The APPLY section 

And if we add an apply section, with a go html-template, like this...

~~~ {#mycode .html}
apply [[
	<div class="selecttrg"> {{ . }} </div> 
]] 
~~~

.. then we get a second webservice, but this one producing html. The {{ . }} will contain the resulting value of the select statement. 

### Query with curl 

Once the backend is compiled, and running, you can easily query it with a CLI tool like CURL: 

~~~ {#mycode .bash}
curl -sS -X POST -d'cceu=LU'  http://localhost:8082/htmxapp2/x01/geteucountrynamejson

"Grand Duchy of Luxembourg"
~~~

Similar action can be done for the HTML producing webservice: 


~~~ {#mycode .bash}
curl -sS -X POST -d'cceu=LU'  http://localhost:8082/htmxapp2/x01/geteucountryname

<div class="selecttrg"> Grand Duchy of Luxembourg </div>
~~~ 


### The APPLYERROR section 

Whenever an error occurs, then instead of the apply template, the applyerror template is executed. 

~~~ {#mycode .html}
applyerror [[
	<div  class="selecttrgerr">  Oh bummer, we got an error: {{ . }} </div> 
]] 
~~~


### The runws directive

To make it quick and convenient to test, you add the parameters that need to be posted when using CURL. 

~~~
runws {{ 
	'cceu=LU' 
}}
~~~



### Finally everything put together

~~~ {#mycode .html}
func GetEuCountryName(cceu string) string
flag:webservice
<<
    select  official_name_en 
    from    t_eu_country
    where   cceu=$1
    and     current_date between eu_entry_date and eu_exit_date
>>
runws {{ 
	'cceu=LU' 
}}
apply [[
	<div class="selecttrg"> {{ . }} </div> 
]] 
applyerror [[
    <div  class="selecttrgerr"> Error: {{ . }} </div>
]] 
~~~


### Compiling

Run the `buildnrun.sh` script. 

### Run the database container



### Run the postgres database 

~~~
docker run -p 45432:5432 -d wmoco/htmxapp2db
~~~

### Run the server

Execute: `./htmxapp2server`

If you get database connection problems then first check if your DB is running, and then check the connection details in `./src/config/configuration.go`. 

### Test the server

Try the curl statements introduced earlier: 

~~~ {#mycode .bash}
curl -sS -X POST -d'cceu=LU'  http://localhost:8082/htmxapp2/x01/geteucountrynamejson
curl -sS -X POST -d'cceu=LU'  http://localhost:8082/htmxapp2/x01/geteucountryname
~~~

### Test the application 

Open this in a web-browser: [wmo.co/htmxapp2/static/htmxapp2.html](http://wmo.co/htmxapp2/static/htmxapp2.html)


