# List of struct

## The left pane 

The backend code of the left pane is similar to the right pane. 

~~~ {#mycode .html}
type EuCountry struct {
    Cceu string
    Cc2  string
    Name string
}

func GetEuCountryList() []EuCountry
flag:webservice
<<
    select  cceu, cc2, short_name 
	from	t_eu_country
	where   current_date between eu_entry_date and eu_exit_date
>>
runws {{ dummy=1  }}
apply [[
    <table>
    {{range .}}
		<tr><td>{{.Cceu}}</td> 
            <td hx-post="/htmxapp2/x03/getcountrycitylist?cc2={{.Cc2}}"
                hx-target="#rightactionpane">{{.Name}}</td>
        </tr>
    {{end}}
    </table>
]]
applyerror [[
	<div  class="err">  Oh bummer, we got an error: {{ . }} </div> 
]] 

~~~


## The right pane 


~~~ {#mycode .html}
type City struct {
    Name string
    Latitude string
    Longitude string
    Elevation int64
}

func GetCountryCityList(cc2 string) []City
flag:webservice
<<
	select name, latitude, longitude, elevation
    from t_gn_city
    where country = $1
    order by elevation desc
    limit 25
>>
runws {{ cc2=BE  }}
apply [[
    <table class="detail">
    <tr> <th class="txt" style="width:50%">Name</th>
         <th class="nbr">Lat</th>
         <th class="nbr">Lon</th>
         <th class="nbr">Elevation</th>
    </tr>
    {{range .}}
            <tr>
                <td class="txt">{{.Name}}</td>
                <td class="nbr">{{.Latitude}}</td>
                <td class="nbr">{{.Longitude}}</td>
                <td class="nbr">{{.Elevation}}</td>
            </tr>
    {{end}}
    </table>
]]
applyerror [[
    <span style="color:red">
    Bummer! Got this error here: "{{ . }}"
    </span>
]]
~~~

