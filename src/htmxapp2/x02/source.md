
# <small>x02:</small>Query producing a list 

If you understand the first example, then understanding this is just a small step up. 

The gist of it comes down to the query .. 

~~~ {#mycode .sql}
    select  cc2 
	from	t_eu_country
	where   current_date between eu_entry_date and eu_exit_date
~~~

.. and the html template applied to the result ..
 
~~~ {#mycode .sql}
    {{range .}}
        {{ . }} &nbsp;
    {{end}}
~~~


# Source

## Frontend 

~~~ {#mycode .html}
	<tr>
		<td valign="top" class="leftpane">

			<div id="leftactionpane" hx-post="/htmxapp2/x02/getcc2list" hx-trigger="click" style="border:silver 1px solid">
				<center> Hit me</center>
			</div>
		</td>
		<td valign="top">
			<div id="rightactionpane">
			</div>
		</td>
	</tr>
~~~

## Backend

~~~ {#mycode .sql}
func GetCc2List() []string
flag:webservice
<<
    select  cc2 
	from	t_eu_country
	where   current_date between eu_entry_date and eu_exit_date
>>
runws {{ dummy=1  }}
apply [[
    {{range .}}
        {{ . }} &nbsp;
    {{end}}
]]
applyerror [[
    <div  class="err">Error: {{ . }} </div>
]] 
~~~
