# Creating a composite 

## The left pane

See the x04 example. 



## The right pane


~~~ {#mycode .go}
type Composite struct {
    Cc2 string
    Title string
	Orderby string
    Order string
    CountryDetail EuCountryDetail
    CityLocationList []CityLocation
}

func GetCountryCityList(cc2 string, orderby string, order string) Composite
flag:webservice
{{
	_result=Composite{Cc2:cc2, Title:"Elevation", Orderby:"elevation", Order:"desc" }
	if order=="asc" { 
		_result.Order="asc" 
	}
	switch orderby { 
	case "name":		_result.Orderby="name" 
	case "latitude":	_result.Orderby="latitude" 
	case "longitude":	_result.Orderby="longitude" 
	default:			_result.Orderby="elevation" 
	}
	orderPredicate:=" order by " + 	_result.Orderby + " " + _result.Order

	_result.CityLocationList,err=GetCityList(cc2, orderPredicate);
	if (err!=nil) {
		return
	}
	_result.CountryDetail,err=GetEuCountryDetail(cc2);
	return
}}
~~~
~~~ {#mycode .html}
runws {{
    'cc2=LU&orderby=name&order=desc'
}} 
apply [[
	<div style="min-height:80px"> 
	   <center><h2> {{ .CountryDetail.Shortname }}</h2> 
		   <em>{{ .CountryDetail.OfficialNameEn }} </em><br/>
		   <small><i>Ordered by {{.Orderby }} / {{if eq .Order "desc"}} descending{{else}} ascending{{end}}. </i></small></center>
	</div>
	
	<table class="detail">
	<tr> <th class="txt" style="width:200px">
				{{ if eq .Orderby "name" }} 
					{{ if eq .Order "asc" }} 
					<a hx-post="/htmxapp2/x06/getcountrycitylist?cc2={{.Cc2}}&orderby=name&order=desc" 
							hx-target="#rightactionpane">
						&#x2BC5; Name
					</a>
					{{ else }} 
					<a hx-post="/htmxapp2/x06/getcountrycitylist?cc2={{.Cc2}}&orderby=name&order=asc" 
							hx-target="#rightactionpane">
						&#x2BC6; Name
					</a>	
					{{ end }}
				{{ else }}
					<span hx-post="/htmxapp2/x06/getcountrycitylist?cc2={{.Cc2}}&orderby=name&order=desc" 
					 hx-target="#rightactionpane" hx-trigger="click"> Name </span>
				{{ end }}
		 </th>
		 <th class="nbr">
				{{ if eq .Orderby "latitude" }} 
					{{ if eq .Order "asc" }} 
					<a hx-post="/htmxapp2/x06/getcountrycitylist?cc2={{.Cc2}}&orderby=latitude&order=desc" 
							hx-target="#rightactionpane">
						&#x2BC5; Latitude
					</a>
					{{ else }} 
					<a hx-post="/htmxapp2/x06/getcountrycitylist?cc2={{.Cc2}}&orderby=latitude&order=asc" 
							hx-target="#rightactionpane">
						&#x2BC6; Latitude
					</a>	
					{{ end }}
				{{ else }}
					<span hx-post="/htmxapp2/x06/getcountrycitylist?cc2={{.Cc2}}&orderby=latitude&order=desc" 
					 hx-target="#rightactionpane" hx-trigger="click"> Latitude </span>
				{{ end }}
		 </th>
		 <th class="nbr">
				{{ if eq .Orderby "longitude" }} 
					{{ if eq .Order "asc" }} 
					<a hx-post="/htmxapp2/x06/getcountrycitylist?cc2={{.Cc2}}&orderby=longitude&order=desc" 
							hx-target="#rightactionpane">
						&#x2BC5; Longitude
					</a>
					{{ else }} 
					<a hx-post="/htmxapp2/x06/getcountrycitylist?cc2={{.Cc2}}&orderby=longitude&order=asc" 
							hx-target="#rightactionpane">
						&#x2BC6; Longitude
					</a>	
					{{ end }}
				{{ else }}
					<span hx-post="/htmxapp2/x06/getcountrycitylist?cc2={{.Cc2}}&orderby=longitude&order=desc" 
					 hx-target="#rightactionpane" hx-trigger="click"> Longitude </span>
				{{ end }}
		 </th>
		 <th class="nbr">
				{{ if eq .Orderby "elevation" }} 
					{{ if eq .Order "asc" }} 
					<a hx-post="/htmxapp2/x06/getcountrycitylist?cc2={{.Cc2}}&orderby=elevation&order=desc" 
							hx-target="#rightactionpane">
						&#x2BC5; Elevation
					</a>
					{{ else }} 
					<a hx-post="/htmxapp2/x06/getcountrycitylist?cc2={{.Cc2}}&orderby=elevation&order=asc" 
							hx-target="#rightactionpane">
						&#x2BC6; Elevation
					</a>	
					{{ end }}
				{{ else }}
					<span hx-post="/htmxapp2/x06/getcountrycitylist?cc2={{.Cc2}}&orderby=elevation&order=desc" 
					 hx-target="#rightactionpane" hx-trigger="click"> Elevation </span>
				{{ end }}
		 </th>
	</tr>
	{{range .CityLocationList }} 
			<tr>
				<td class="txt" >{{.Name}}</td> 
				<td class="nbr">{{.Latitude}}</td>
				<td class="nbr">{{.Longitude}}</td> 
				<td class="nbr">{{.Elevation}}</td>
			</tr>
	{{end}}
	</table>
]]
applyerror [[
	<span style="color:red">
	Bummer! Got this error here: "{{ . }}"
	</span>
]]
~~~


## GetEuCountryDetail

~~~ {#mycode .go}
type EuCountryDetail struct {
    Id string
    Shortname string
    ShortnameEn string
    OfficialNameEn string
}


func GetEuCountryDetail(cc2 string) EuCountryDetail
<<
	select cceu, short_name, short_name_en, official_name_en
	from t_eu_country
	where cc2=$1
>>
~~~


## GetCityList

~~~ {#mycode .go}
type CityLocation struct {
    Name string
    Latitude string
    Longitude string
    Elevation int64
}


func GetCityList(cc2 string, order_predicate injection) []CityLocation
<<
	 select name, latitude, longitude, elevation
	 from  t_gn_city 
	 where country = $1
	 order_predicate
	 limit 25
>>
~~~
