package x08

import (
    "fmt"
    "github.com/wcharczuk/go-chart"
    //"github.com/wcharczuk/go-chart/drawing"
    "bytes"
    "math"
)

func Getsvgchart( dataset [][]Point, legendlist []string ) (rv string, err error) {
    aspercentage:=true
    trace:=true;
    if trace { fmt.Println("Gtsvgchart") } 

    var max,min float64
    max=-math.MaxFloat64
    min=math.MaxFloat64

    var serieslist []chart.Series
    for i,pl := range ( dataset) { 
        if trace { fmt.Printf("Getsvgchart: handling dataset #%d\n",i); } 
        if pl==nil || len(pl)==0 { 
            continue
        }
        var xl []float64
        var yl []float64
        y0:=pl[0].Y
        for _,r:=range(pl) { 
            xl = append(xl, r.X)
            y:=r.Y
            if aspercentage { 
                y=(r.Y-y0)/y0*100.0
            }
            yl = append(yl, y)
            if y<min {
                min=y
            }
            if y>max {
                max=y
            }
        }
        serieslist=append(serieslist, chart.ContinuousSeries{ Name: legendlist[i], 
            XValues: xl, YValues: yl, 
            Style: chart.Style{ StrokeWidth: 2.0, },
        })
    }

    diff:=max-min
    max=max+0.1*diff;
    min=min-0.1*diff;


    // the horizontal line at 0%  
    hline := &chart.ContinuousSeries{
            Style: chart.Style{
                StrokeColor:     chart.ColorAlternateBlue,
                StrokeDashArray: []float64{5.0, 5.0},
                StrokeWidth: 1.0,
            },
            XValues: []float64{ 2018.0, 2100.0 }, 
            YValues: []float64{ 0.0, 0.0 }, 
        }
    serieslist=append(serieslist, hline )
    serieslist=append(serieslist, chart.LastValueAnnotationSeries(hline) )


    graph := chart.Chart{
        Background: chart.Style{
            Padding: chart.Box{
                Top:  20,
                Left: 40,
                Right: 20,
            },
        },
        XAxis: chart.XAxis{
            //TickPosition: chart.TickPositionBetweenTicks,
            Name: "Year",
            ValueFormatter: func(v interface{}) string {
                if vf, isFloat := v.(float64); isFloat {
                    return fmt.Sprintf("%4.0f", vf)
                }
                return ""
            },
        },
        YAxis: chart.YAxis{
            Name: "Population Growth",
            Range: &chart.ContinuousRange{
                Max: max,
                Min: min,
            },
            ValueFormatter: func(v interface{}) string {
                if vf, isFloat := v.(float64); isFloat {
                    return fmt.Sprintf("%3.0f%%", vf)
                }
                return ""
            },
        },
        Series: serieslist, 
    }

    // extra, separate step for the legend, because ref to graph needed
    graph.Elements = []chart.Renderable{
        chart.LegendLeft(&graph),
    }

    var bb bytes.Buffer
    graph.Render(chart.SVG, &bb)
    rv=bb.String()
    return
}
