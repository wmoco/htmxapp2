package config

import (
	"database/sql"
	"fmt"
	_ "github.com/lib/pq"
	"os"
)

var gdbh *sql.DB=nil

func GetDBH() (dbh *sql.DB, err error) {
	err=nil
	if (gdbh==nil)  { 
		//LOCAL 
		gdbh, err = sql.Open("postgres", "host=databeast port=5432 dbname=htmxapp2p user=pewpew password=pew99pew sslmode=disable")
		// container
		//gdbh, err = sql.Open("postgres", "host=localhost port=45432 dbname=htmxapp2db user=cewcew password=cew99cew99 sslmode=disable")

		fmt.Println("opening connection");
	}
	dbh=gdbh
	return 
}


func LogErr(err error, msg string) {
	fmt.Fprintf(os.Stderr, "%s:%s\n", msg, err.Error())
}

