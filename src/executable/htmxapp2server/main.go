
package main

import (
    "fmt"
    "log"
    "net/http"
    "time"
    "htmxapp2/x01"
    "htmxapp2/x02"
    "htmxapp2/x03"
    "htmxapp2/x04"
    "htmxapp2/x05"
    "htmxapp2/x06"
//  "htmxapp2/x07"
    "htmxapp2/x08"
)

func main() {

    mux := http.NewServeMux()
    x01.RegisterWebServices(mux, 2)
    x02.RegisterWebServices(mux, 2)
    x03.RegisterWebServices(mux, 2)
    x04.RegisterWebServices(mux, 2)
    x05.RegisterWebServices(mux, 2)
    x06.RegisterWebServices(mux, 2)
    //x07.RegisterWebServices(mux, 2)
    x08.RegisterWebServices(mux, 2)

    mux.Handle("/htmxapp2/static/",
        http.StripPrefix("/htmxapp2/static/",
            http.FileServer(http.Dir("./static"))))

    fmt.Println("Serving")

    srv := &http.Server{
        Addr:         ":8082",
        Handler:      mux,
        ReadTimeout:  5 * time.Second,
        WriteTimeout: 10 * time.Second,
    }
    log.Println(srv.ListenAndServe())
}



